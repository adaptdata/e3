# e3

This repository contains data extracted from Engagement 3 of the DARPA
*Transparent Computing* Program.  This was an exercise in which
several provenance tracking systems were configured in a network, to
run background activity workloads, while a red team carried out
Advanced Persistent Threat style attacks.  This dataset had just one
scenario which we call Main, and several recording systems were used
running on different operating systems.  Among those used in the TC
program, this dataset includes data for the `main` scenario
from five teams, which are named according to the respective TC project team names.

* `5dir`: a Windows-based provenance tracking tool developed by the
  FiveDirections project.
* `cadets`: a BSD-based provenance tracking tool developed by the
CADETS project.
* `clearscope`: an Android-based provenance tracking tool developed by
the ClearScope project.
* `theia`: a Linux-based provenance tracking tool developed by the
  TRACE project.
* `trace`: a Linux-based provenance tracking tool developed by the
  THEIA project.

The raw data is available [here](https://github.com/darpa-i2o/Transparent-Computing).  The raw data was
processed by the ingester developed by the ADAPT Project, which is
available [here](https://github.com/GaloisInc/Adapt).  The ingester performs some deduplication and
integration and stores the results in a Neo4j graph database, which
can be queried using Cypher.  We subsequently extracted several
feature views, or *contexts*, for the purpose of evaluating generic
approaches to anomaly detection to see whether the attack data could
be found using general-purpose techniques.  The datasets, and baseline
experiments measuring the performance of different anomaly detectors
and rank aggregation techniques,
can be found in these papers:

* Ghita Berrada, James Cheney, Sidahmed Benabderrahmane, William Maxwell, Himan Mookherjee, Alec Theriault, Ryan Wright,
[A baseline unsupervised advanced persistent threat detection in system-level provenance](https://arxiv.org/abs/1906.06940),
Future Generation Computer Systems,
Volume 108,
2020,
Pages 401-413
* Ghita Berrada and James Cheney.  [Aggregating unsupervised
provenance anomaly detectors](https://www.usenix.org/system/files/tapp2019-paper-berrada.pdf).  TaPP 2019

The Engagement 3 Main datasets correspond to "Scenario 2" in these papers.

Please cite the FGCS paper (which provides the most detail about the data) if reusing this dataset.

Contexts are stored as .csv files in which the first column is an
object identifier (typically, a UUID from the underlying dataset) and
the remaining columns are binary-valued features (0=absent/1=present).
The contexts in each subdirectory are as follows:

* `ProcessEvent`: The
attributes are possible event types performed by the process.
* `ProcessExec`: The attributes are executable names or command lines
used to start the process.
* `ProcessParent`: The attributes are executable names or command
lines used to start the *parent* of the process.
* `ProcessNetflow`: The attributes are IP addresses or port numbers
ever *accessed* (opened/closed/read/written) by the process.
* `ProcessAll`: The attributes are the disjoint union of the four
  basic contexts.  Attributes are renamed to avoid collision for
  example between `ProcessExec` and `ProcessParent`.

Context .csv files are also gzipped to save repository space/network
bandwidth since some of them are very sparse.

Each directory also contains one or more "ground truth" files with
names like `<provider>_main.csv`.  For example, the file
`5dir_main.csv` provides ground truth for the `5dir` provider.  The ground truth
files collect UUIDs and node types for parts of attacks.  These were painstakingly 
constructed by team members at the University of Edinburgh
 as described in the above paper.  Each row contains a
UUID of an element of the ingested graph and a node type.  For the
contexts we are currently providing, only the node type
`AdmSubject::Node` is meaningful, since this is the type used for
process nodes; other node types are not present in the process-centric
contexts that are currently present in the repository.
Some scenarios have
more than one distinct attack stage but there is sometimes overlap
between the stages.

